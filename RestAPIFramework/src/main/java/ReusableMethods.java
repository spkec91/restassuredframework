import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public class ReusableMethods {
    public static String getSessionKey() throws IOException {
        Logger logger = LoggerFactory.getLogger("sample");
        Response res = given().header
                ("Content-Type", "application/json").
/*                body("{\n" +
                        "    \"username\": \"spkec91\",\n" +
                        "    \"password\": \"Polo@123\"\n" +
                        "}").when().*/
        body(forBody()).when().
                        post(readFromProperties.reads("auth")).then().statusCode(200).extract().response();
        JsonPath jp = rawtoJson(res);
        String sessionid = jp.get("session.value");
        logger.info("Session id is : " + sessionid);
        return sessionid;
    }

    public static JsonPath rawtoJson(Response res) {
        String s = res.asString();
        JsonPath j = new JsonPath(s);
        return j;
    }

    public static JSONObject forBody() {
        JSONObject requestParams = new JSONObject();
        requestParams.put("username", "spkec91");
        requestParams.put("password", "Polo@123");
        return requestParams;
    }

}
