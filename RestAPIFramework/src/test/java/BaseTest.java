import io.qameta.allure.Allure;
import io.qameta.allure.Issue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;


public class BaseTest {

    @Issue("BeforeSuite")
    @BeforeSuite
    public void setUp() throws IOException {
        Logger logger = LoggerFactory.getLogger(this.getClass());
        ApiUtils.setBaseUri(readFromProperties.reads("baseuri"));
        logger.info("baseuri is : " + readFromProperties.reads("baseuri"));


    }

    @Issue("AfterSuite")
    @AfterSuite
    public void cleanUp() {
        Allure.step("resetting baseURI");
        Allure.story("");
        ApiUtils.resetBaseUri();

    }
}
