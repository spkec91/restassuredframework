import io.qameta.allure.Allure;
import io.qameta.allure.Issue;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public class TestCases extends BaseTest {
    Logger logger = LoggerFactory.getLogger("sample");

    @Issue("create JIRA Issue and get the Assignee name of the same issue")
    @Test(enabled = true)
    public void createIssue() throws IOException {
        Response res = given().header("Content-Type", "application/json").
                header("Cookie", "JSESSIONID=" + ReusableMethods.getSessionKey()).
                body("{\n" +
                        "\"fields\": {\n" +
                        "   \"project\":\n" +
                        "   { \n" +
                        "      \"key\": \"MYP\"\n" +
                        "   },\n" +
                        "   \"summary\": \"REST EXAMPLE\",\n" +
                        "   \"description\": \"Creating an issue via REST API\",\n" +
                        "   \"issuetype\": {\n" +
                        "      \"name\": \"Bug\"\n" +
                        "   }\n" +
                        "  }\n" +
                        "}").
                when().
                post(readFromProperties.postResource("createIssue")).then().statusCode(201).extract().response();
        JsonPath j = ReusableMethods.rawtoJson(res);
        System.out.println("response as string : " + res.asString());
        System.out.println("Issue created with id : " + j.get("id"));

        Allure.story("Get Assignee value");
        logger.info("Extract the id and pass it get call");
        Response res1 = given().header("Content-Type", "application/json").
                header("Cookie", "JSESSIONID=" + ReusableMethods.getSessionKey()).
                when().
                get("/rest/api/2/issue/" + j.get("id")).then().statusCode(200).extract().response();
        System.out.println("response as string : " + res.asString());
        JsonPath j1 = ReusableMethods.rawtoJson(res);
        logger.info("Assignee is : " + j1.get("assignee"));

    }

    @Test(enabled = true)
    @Parameters("deleteissue")
    @Issue("Deleting the Issue based on id")
    public void deleteIssue(String deleteissue) throws IOException {
        Response res = given().header("Cookie", "JSESSIONID=" + ReusableMethods.getSessionKey()).when().delete("/rest/api/2/issue/" + deleteissue).then().statusCode(204).extract().response();
    }

    @Test(enabled = true)
    @Parameters("id")
    @Issue("Get the details of any issue based on id")
    public void issueDetails(String id) throws IOException {
        Response res = given().header("Content-Type", "application/json").
                header("Cookie", "JSESSIONID=" + ReusableMethods.getSessionKey()).
                when().
                get("/rest/api/2/issue/" + id).then().statusCode(200).extract().response();
        logger.info("response as string : " + res.asString());
    }
}


