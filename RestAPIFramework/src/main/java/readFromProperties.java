import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class readFromProperties {
    public static String reads(String str) throws IOException {
        FileReader reader=new FileReader("src/main/resources/input.properties");

        Properties p=new Properties();
        p.load(reader);
        return p.getProperty(str);
    }
    public static String postResource(String str) throws IOException {
        FileReader reader=new FileReader("src/main/resources/postResource.properties");

        Properties p=new Properties();
        p.load(reader);
        return p.getProperty(str);
    }
}

