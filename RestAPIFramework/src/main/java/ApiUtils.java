import io.restassured.RestAssured;

public class ApiUtils {
    public static void setBaseUri(String baseUri) {
        RestAssured.baseURI = baseUri;
    }

    public static void resetBaseUri() {
        RestAssured.baseURI = "";

    }
}
